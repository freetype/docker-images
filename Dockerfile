FROM debian:buster-20210511-slim

RUN set -x && \
    apt-get update && apt-get upgrade -qqy && \
    apt-get install -qqy --no-install-suggests --no-install-recommends \
    git automake autoconf libtool make cmake gcc g++ clang llvm \
    python3 python3-pip libx11-dev libpng-dev \
    zlib1g-dev libbz2-dev libharfbuzz-dev tree libbrotli-dev && \
    python3 -m pip install --upgrade setuptools pip && \
    python3 -m pip install meson ninja opencv-python-headless docwriter --upgrade
